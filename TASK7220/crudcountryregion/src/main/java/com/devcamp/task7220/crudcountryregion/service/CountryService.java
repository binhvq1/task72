package com.devcamp.task7220.crudcountryregion.service;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.task7220.crudcountryregion.model.Country;
import com.devcamp.task7220.crudcountryregion.repository.ICountryRepository;

@Service
public class CountryService {
    //////////////////////////// IMPOTR INTERFACE///////////////////////////
    @Autowired
    private ICountryRepository countryRepository;

    //////////////////////////// GET SERVICES///////////////////////////
    public ResponseEntity<List<Country>> getAllCountries() {
        try {
            List<Country> pCountrys = new ArrayList<Country>();

            countryRepository.findAll().forEach(pCountrys::add);

            return new ResponseEntity<>(pCountrys, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> getCountryById(long id) {
        Optional<Country> drinkData = countryRepository.findById(id);
        if (drinkData.isPresent()) {
            return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //////////////////////////// POST SERVICES///////////////////////////
    public ResponseEntity<Object> createCountry(Country Country) {
        try {

            Optional<Country> CountryData = countryRepository.findById(Country.getId());
            if (CountryData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
            }

            Country newCountry = countryRepository.save(Country);
            return new ResponseEntity<>(newCountry, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
    }

    //////////////////////////// PUT SERVICES///////////////////////////
    public ResponseEntity<Object> updateCountryById(long id, Country Country) {
        Optional<Country> CountryData = countryRepository.findById(id);
        if (CountryData.isPresent()) {
            Country CountryUpdate = CountryData.get();
            CountryUpdate.setCountryCode(Country.getCountryCode());
            CountryUpdate.setCountryName(Country.getCountryName());

            try {
                return new ResponseEntity<>(countryRepository.save(CountryUpdate), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
        }
    }

    //////////////////////////// DELETE SERVICES///////////////////////////
    public ResponseEntity<Object> deleteCountryById(long id) {
        try {
            countryRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteCountries() {
        try {
            countryRepository.deleteAll();
            ;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
