package com.devcamp.task7220.crudcountryregion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task7220.crudcountryregion.model.Region;

import com.devcamp.task7220.crudcountryregion.service.RegionService;

@RestController
@RequestMapping("/region")
public class RegionController {
    @Autowired
    private RegionService regionService;

    @GetMapping
    public ResponseEntity<List<Region>> getAllRegions() {
        ResponseEntity<List<Region>> responseRegions = regionService.getAllRegions();
        return responseRegions;
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getRegionById(@PathVariable("id") long id) {
        ResponseEntity<Object> response = regionService.getRegionById(id);
        return response;
    }

    @GetMapping("/country")
    public ResponseEntity<Object> getRegionByUserId(@RequestParam(value = "id") long id) {
        ResponseEntity<Object> response = regionService.getRegionByCountry(id);
        return response;
    }

    //////////////// POST MAPPING ///////////////
    @PostMapping("/country")
    public ResponseEntity<Object> createRegion(@RequestParam(value = "id") Long id, @RequestBody Region Region) {
        ResponseEntity<Object> responseRegions = regionService.createRegion(id, Region);
        return responseRegions;
    }

    //////////////// PUT MAPPING ///////////////
    @PutMapping("{id}")
    public ResponseEntity<Object> updateRegionById(@PathVariable Long id, @RequestBody Region Region) {
        ResponseEntity<Object> responseRegions = regionService.updateRegionById(id, Region);
        return responseRegions;
    }

    //////////////// DELETE MAPPING ///////////////
    @DeleteMapping()
    public ResponseEntity<Object> deleteRegions() {
        ResponseEntity<Object> responseRegions = regionService.deleteRegions();
        return responseRegions;
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
        ResponseEntity<Object> responseRegions = regionService.deleteRegionById(id);
        return responseRegions;
    }
}