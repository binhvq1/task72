package com.devcamp.task7220.crudcountryregion.service;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.task7220.crudcountryregion.model.Country;
import com.devcamp.task7220.crudcountryregion.model.Region;

import com.devcamp.task7220.crudcountryregion.repository.IRegionRepository;
import com.devcamp.task7220.crudcountryregion.repository.ICountryRepository;

@Service
public class RegionService {
    @Autowired
    private ICountryRepository countryRepository;
    @Autowired
    private IRegionRepository regionRepository;

    //////////////////////////// GET SERVICES///////////////////////////
    public ResponseEntity<List<Region>> getAllRegions() {
        try {
            List<Region> pRegions = new ArrayList<Region>();

            regionRepository.findAll().forEach(pRegions::add);

            return new ResponseEntity<>(pRegions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> getRegionById(long id) {
        Optional<Region> drinkData = regionRepository.findById(id);
        if (drinkData.isPresent()) {
            return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Object> getRegionByCountry(long countryid) {
        try {
            // Long id = Long.parseLong(userId);
            Optional<Country> vCustomer = countryRepository.findById(countryid);
            if (vCustomer != null) {
                return new ResponseEntity<>(vCustomer.get().getRegions(), HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    //////////////////////////// POST SERVICES///////////////////////////
    public ResponseEntity<Object> createRegion(Long id, Region region) {
        try {

            Optional<Region> RegionData = regionRepository.findById(region.getId());
            Optional<Country> countryData = countryRepository.findById(id);
            if (RegionData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
            }
            region.setCountry(countryData.get());

            Region newRegion = regionRepository.save(region);

            return new ResponseEntity<>(newRegion, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
    }

    //////////////////////////// PUT SERVICES///////////////////////////
    public ResponseEntity<Object> updateRegionById(long id, Region region) {
        Optional<Region> regionData = regionRepository.findById(id);
        if (regionData.isPresent()) {
            Region RegionUpdate = regionData.get();
            RegionUpdate.setRegionName(region.getRegionName());
            RegionUpdate.setRegionCode(region.getRegionCode());

            try {
                return new ResponseEntity<>(regionRepository.save(RegionUpdate), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
        }
    }

    //////////////////////////// DELETE SERVICES///////////////////////////
    public ResponseEntity<Object> deleteRegionById(long id) {
        try {
            regionRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteRegions() {
        try {
            regionRepository.deleteAll();
            ;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
