package com.devcamp.task7220.crudcountryregion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.task7220.crudcountryregion.model.Country;
import com.devcamp.task7220.crudcountryregion.service.CountryService;

@RestController
@RequestMapping("/country")
public class CountryController {
    //////////////// IMPORT SERVICES ///////////////
    @Autowired
    private CountryService countryService;

    //////////////// GET MAPPING ///////////////
    @GetMapping
    public ResponseEntity<List<Country>> getAllCountrys() {
        ResponseEntity<List<Country>> responseCountrys = countryService.getAllCountries();
        return responseCountrys;
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getCountryById(@PathVariable("id") long id) {
        ResponseEntity<Object> response = countryService.getCountryById(id);
        return response;
    }

    //////////////// POST MAPPING ///////////////
    @PostMapping()
    public ResponseEntity<Object> createCountry(@RequestBody Country Country) {
        ResponseEntity<Object> responseCountrys = countryService.createCountry(Country);
        return responseCountrys;
    }

    //////////////// PUT MAPPING ///////////////
    @PutMapping("{id}")
    public ResponseEntity<Object> updateCountryById(@PathVariable Long id, @RequestBody Country Country) {
        ResponseEntity<Object> responseCountrys = countryService.updateCountryById(id, Country);
        return responseCountrys;
    }

    //////////////// DELETE MAPPING ///////////////
    @DeleteMapping()
    public ResponseEntity<Object> deleteCountrys() {
        ResponseEntity<Object> responseCountrys = countryService.deleteCountries();
        return responseCountrys;
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
        ResponseEntity<Object> responseCountrys = countryService.deleteCountryById(id);
        return responseCountrys;
    }
}
