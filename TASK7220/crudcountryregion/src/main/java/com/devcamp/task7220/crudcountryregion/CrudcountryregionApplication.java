package com.devcamp.task7220.crudcountryregion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudcountryregionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudcountryregionApplication.class, args);
	}

}
