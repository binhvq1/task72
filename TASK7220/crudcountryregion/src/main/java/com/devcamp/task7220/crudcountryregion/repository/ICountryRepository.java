package com.devcamp.task7220.crudcountryregion.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task7220.crudcountryregion.model.Country;

public interface ICountryRepository extends JpaRepository<Country, Long> {
}
