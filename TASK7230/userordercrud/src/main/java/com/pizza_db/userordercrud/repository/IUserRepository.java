package com.pizza_db.userordercrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizza_db.userordercrud.model.CUser;


public interface IUserRepository extends JpaRepository<CUser, Long>{
    
}
