package com.pizza_db.userordercrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserordercrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserordercrudApplication.class, args);
	}

}
